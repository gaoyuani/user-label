package com.mzdata.label.service;

import com.mzdata.label.model.param.UserTagParam;
import com.mzdata.label.model.vo.BasicUserTagVO;
import com.mzdata.label.model.vo.UserTagVO;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:44
 */
public interface TagService {

    List<UserTagVO> getRelevanceTagList();

    List<BasicUserTagVO> getBasicsTagList();

    int createUserTag(UserTagParam param);
}
