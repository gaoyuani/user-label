package com.mzdata.label.service;

import com.mzdata.label.model.entity.UserTagCategory;
import com.mzdata.label.model.param.TagCategoryParam;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/20 12:18
 */
public interface TagCategoryService {

    int createTagCategory(TagCategoryParam param);

    List<UserTagCategory> getTagCategoryList(Integer type, long tenantId);
}
