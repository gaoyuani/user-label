package com.mzdata.label.service.impl;

import com.google.common.collect.Lists;
import com.mzdata.label.context.UserInfoHolder;
import com.mzdata.label.mapper.TagMapper;
import com.mzdata.label.model.entity.UserTag;
import com.mzdata.label.model.param.UserTagParam;
import com.mzdata.label.model.vo.BasicUserTagVO;
import com.mzdata.label.model.vo.UserTagVO;
import com.mzdata.label.service.TagService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:45
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    @Override
    public List<UserTagVO> getRelevanceTagList() {
        List<UserTagVO> list = Lists.newArrayList();
        long tenantId = UserInfoHolder.getInfo().getTenantId();
        List<UserTag> allTagList = tagMapper.getAllTagList(tenantId, 1);
        UserTagVO userTagVO;
        if(!allTagList.isEmpty()){
            Map<Long, List<UserTag>> collect = allTagList.stream().collect(Collectors.groupingBy(UserTag::getCategoryId));
            List<UserTagVO.UserTagView> tagViewList;
            UserTagVO.UserTagView view;
            for (List<UserTag> m : collect.values()) {
                UserTag userTag = m.get(0);
                userTagVO = new UserTagVO();
                userTagVO.setLabel(userTag.getCategoryName());
                userTagVO.setValue(userTag.getCategoryId());
                tagViewList = Lists.newArrayList();
                for (UserTag tag : m) {
                    view = new UserTagVO.UserTagView();
                    view.setTenantId(tag.getTenantId());
                    view.setLabel(tag.getName());
                    view.setValue(tag.getId());
                    view.setDesc(tag.getComment());
                    tagViewList.add(view);
                }
                userTagVO.setList(tagViewList);
                list.add(userTagVO);
            }
        }
        return list;
    }

    @Override
    public List<BasicUserTagVO> getBasicsTagList() {
        List<BasicUserTagVO> list = Lists.newArrayList();
        long tenantId = UserInfoHolder.getInfo().getTenantId();
        List<UserTag> allTagList = tagMapper.getAllTagList(tenantId, 1);
        if(!allTagList.isEmpty()){
            allTagList.forEach(p -> {
                BasicUserTagVO basicUserTagVO = new BasicUserTagVO();
                BeanUtils.copyProperties(p, basicUserTagVO);
                list.add(basicUserTagVO);
            });
        }
        return list;
    }

    @Override
    public int createUserTag(UserTagParam param) {
        UserTag userTag = tagMapper.selectByPrimaryKey(param.getId());
        userTag.setId(0);
        userTag.setFromId(param.getFromId());
        /*userTag.setSqlParameters();
        userTag.setSqlTemplate();
        userTag.setSqlValues();
        userTag.setSql();*/
        return tagMapper.saveUserTag(userTag);
    }
}
