package com.mzdata.label.service.impl;

import com.alibaba.fastjson.JSON;
import com.mzdata.label.context.UserInfoHolder;
import com.mzdata.label.mapper.TagCategoryMapper;
import com.mzdata.label.model.entity.UserTagCategory;
import com.mzdata.label.model.param.TagCategoryParam;
import com.mzdata.label.service.TagCategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/20 12:20
 */
@Service
public class TagCategoryServiceImpl implements TagCategoryService {

    @Autowired
    private TagCategoryMapper tagCategoryMapper;

    @Override
    public int createTagCategory(TagCategoryParam param) {
        //创建标签分类
        UserTagCategory category = new UserTagCategory();
        BeanUtils.copyProperties(param, category);

        category.setTenantId(UserInfoHolder.getInfo().getTenantId());
        category.setType(1);

        UserTagCategory parentInfo = tagCategoryMapper.selectByPrimaryKey(param.getParentId());
        category.setLevel(parentInfo.getLevel() + 1);

        int maxSortValue = tagCategoryMapper.selectMaxSortValueByParentId(param.getParentId());
        category.setSortValue(maxSortValue + 1);
        category.setStatus(1);
        category.setUserCount(0);
        category.setUserPercentage(new BigDecimal(0));
        category.setBaseTagIds(JSON.toJSONString(param.getBaseTagIds()));
        return tagCategoryMapper.saveTagCategory(category);
    }

    @Override
    public List<UserTagCategory> getTagCategoryList(Integer type, long tenantId) {
        List<UserTagCategory> userTagCategories = tagCategoryMapper.selectByTenantIdAndType(tenantId, type);
        if(!userTagCategories.isEmpty()){
            if(type != 0){
                userTagCategories = fixData(userTagCategories);
            }else{
                //如果type为0，则基础标签、复合标签的数据需要分开来展示
                List<UserTagCategory> basicList   = userTagCategories.stream().filter(p -> p.getType() == 1).collect(Collectors.toList());
                List<UserTagCategory> complexList = userTagCategories.stream().filter(p -> p.getType() == 2).collect(Collectors.toList());

                List<UserTagCategory> result    = new ArrayList<>();
                UserTagCategory basicCategory   = new UserTagCategory();

                basicCategory.setChildren(fixData(basicList));
                result.add(basicCategory);

                UserTagCategory complexCategory = new UserTagCategory();
                complexCategory.setChildren(fixData(complexList));
                result.add(complexCategory);

                userTagCategories = result;
            }
        }

        return userTagCategories.stream().filter(p -> p.getParentId() ==0).collect(Collectors.toList());
    }

    public List<UserTagCategory> fixData(List<UserTagCategory> userTagCategories){
        userTagCategories.stream().filter(category ->
                category.getParentId() == 0
        ).map((meun) -> {
            //2、调用查询子数据
            meun.setChildren(child(meun,userTagCategories));
            return meun;
        }).sorted((menu1,meun2) ->{
            // 排序可自定义
            return (menu1.getSortValue() == null?0:menu1.getSortValue()) - (meun2.getSortValue() == null?0:meun2.getSortValue());
        }).collect(Collectors.toList());
        return userTagCategories;
    }

    private List<UserTagCategory> child(UserTagCategory root,List<UserTagCategory> pmsCategories){
        List<UserTagCategory> collect = pmsCategories.stream().filter(category ->
            category.getParentId() == root.getId()
        ).map((category) -> {
            // 找到子数据,注意这里式递归查找,子集有可能还有子集
            category.setChildren(child(category, pmsCategories));
            return category;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSortValue() == null?0:menu1.getSortValue()) - (menu2.getSortValue() == null?0:menu2.getSortValue());
        }).collect(Collectors.toList());
        return collect;
    }
}
