package com.mzdata.label.context;

import com.mzdata.label.model.bo.AuthorUserInfoBo;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:27
 */
public class UserInfoHolder {
    private static final ThreadLocal<AuthorUserInfoBo> USER_INFO = new ThreadLocal<>();

    public static void setInfo(AuthorUserInfoBo baseInfoBo) {
        USER_INFO.set(baseInfoBo);
    }

    public static AuthorUserInfoBo getInfo() {
        return USER_INFO.get();
    }

    public static void remove() {
        USER_INFO.remove();
    }
}
