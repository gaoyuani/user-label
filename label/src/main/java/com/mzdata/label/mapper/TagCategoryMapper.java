package com.mzdata.label.mapper;

import com.mzdata.label.model.entity.UserTagCategory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/20 14:29
 */

@Repository
public interface TagCategoryMapper {

    UserTagCategory selectByPrimaryKey(long id);

    int selectMaxSortValueByParentId(long parentId);

    int saveTagCategory(UserTagCategory category);

    List<UserTagCategory> selectByTenantIdAndType(Long tenantId, Integer type);
}
