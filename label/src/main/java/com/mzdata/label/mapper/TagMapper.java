package com.mzdata.label.mapper;

import com.mzdata.label.model.entity.UserTag;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:46
 */
@Repository
public interface TagMapper {

    UserTag selectByPrimaryKey(long id);

    int saveUserTag(UserTag userTag);

    List<UserTag> getAllTagList(long tenantId, Integer type);
}
