package com.mzdata.label.controller;

import com.mzdata.label.http.JsonResult;
import com.mzdata.label.model.param.UserTagParam;
import com.mzdata.label.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:09
 */
@RestController
@RequestMapping("/userLabel/api/v1/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    /**
     * 添加标签分类时关联的标签列表
     * @return
     */
    @GetMapping("getRelevanceTagList")
    public JsonResult getRelevanceTagList(){
        return JsonResult.success(tagService.getRelevanceTagList());
    }

    /**
     * 基础标签管理列表
     * @return
     */
    @GetMapping("getBasicsTagList")
    public JsonResult getBasicsTagList(){
        return JsonResult.success(tagService.getBasicsTagList());
    }

    /**
     * 基础标签复制或修改
     * @param param
     * @return
     */
    @PostMapping("copyBasicsUserTag")
    public JsonResult copyBasicsUserTag(@RequestBody UserTagParam param){
        return JsonResult.success(tagService.createUserTag(param));
    }

}
