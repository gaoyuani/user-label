package com.mzdata.label.controller;

import com.mzdata.label.context.UserInfoHolder;
import com.mzdata.label.http.JsonResult;
import com.mzdata.label.model.param.TagCategoryParam;
import com.mzdata.label.service.TagCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/22 14:23
 */
@RestController
@RequestMapping("/userLabel/api/v1/tagCategory")
public class TagCategoryController {

    @Autowired
    private TagCategoryService tagCategoryService;

    @GetMapping("getTagCategoryList")
    public JsonResult getTagCategoryList(Integer type){
        return JsonResult.success(tagCategoryService.getTagCategoryList(type, UserInfoHolder.getInfo().getTenantId()));
    }

    @PostMapping("createTagCategory")
    public JsonResult createTagCategory(@RequestBody TagCategoryParam param){
        return JsonResult.success(tagCategoryService.createTagCategory(param));
    }

}
