package com.mzdata.label.http;


import com.alibaba.fastjson.JSON;

import java.io.Serializable;

public class JsonResult implements Serializable {

    private static final long serialVersionUID = -2881030698070873511L;
    private int code;
    private String error;
    private Object result;
    private Integer total;

    public static JsonResult success(Object data) {
        return new JsonResult(1, "", data);
    }

    public static JsonResult success(Object data, Integer total) {
        return new JsonResult("", data, 1, total);
    }

    public static JsonResult bizError(String errorMsg) {
        return new JsonResult(errorMsg, null, -1024);
    }

    public static JsonResult bizError(int code, String errorMsg) {
        return new JsonResult(errorMsg, null, code);
    }

    public static JsonResult error(String errorMsg) {
        return new JsonResult(0, errorMsg, null);
    }

    public static JsonResult error(int code, String errorMsg) {
        return new JsonResult(errorMsg, null, code);
    }

    public JsonResult() {
    }

    public JsonResult(int code, String error, Object result) {
        this.code = code;
        this.error = error;
        this.result = result;
    }

    public JsonResult(String error, Object result, int code) {
        this.error = error;
        this.result = result;
        this.code = code;
    }

    public JsonResult(String error, Object result, int code, Integer total) {
        this.error = error;
        this.result = result;
        this.code = code;
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
