package com.mzdata.label.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mzdata.label.model.bo.AuthorUserInfoBo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author ：lxh
 * @description：
 * @date ：2019/11/19 11:32
 */
@Service
@Slf4j
public class AuthorizationService {

  @Autowired
  private RestTemplate restTemplate;

  @Value("${auth_get_tenant_id_url}")
  private String getTenantIdURL;

  private LoadingCache<String, String> token = CacheBuilder.newBuilder()
      .maximumSize(10)
      .expireAfterWrite(14, TimeUnit.DAYS)
      .build(
          new CacheLoader<String, String>() {
            public String load(String key) {
              return genToken(key);
            }
          });

  public Long getTenantId(HttpServletRequest request) {
    long tenantId = 0;
    String token = request.getParameter("token");
    if(StringUtils.isBlank(token)){
      Cookie[] cookies = request.getCookies();
      if (cookies != null && cookies.length >= 1) {
        for (Cookie cookie : cookies) {
          String name = cookie.getName();
          if ("token".equals(name)) {
            token = " bearer " + cookie.getValue();
            break;
          }
        }
      }

      if(StringUtils.isBlank(token)){
        String authorizeSign = request.getHeader("Authorization");
        if(StringUtils.isNotBlank(authorizeSign)){
          token = authorizeSign;
        }
      }
    }

    if (StringUtils.isNotBlank(token)) {
      // 取权限系统查询tenantId
      String url = getTenantIdURL + "?token=" + token;
      HttpHeaders headers = new HttpHeaders();
      headers.set("Authorization", token);
      ResponseEntity<JSONObject> resp = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<String>(headers), JSONObject.class);
      log.info("queryUserByToken resp:" + "    token= " + token + "  data:" + JSON.toJSONString(resp.getBody()));
      JSONObject body = resp.getBody();
      if (body != null) {
        Integer code = body.getInteger("code");
        if (code == 0) {
          JSONObject data = body.getJSONObject("data");
          tenantId = data.getLong("tenantId");
        }
      }
    }
    return tenantId;
  }

  public AuthorUserInfoBo getAuthUserInfo(String token) {
    if (StringUtils.isNotBlank(token)) {
      // 取权限系统查询tenantId
      String url = getTenantIdURL + "?token=" + token;
      HttpHeaders headers = new HttpHeaders();
      headers.set("Authorization", token);
      ResponseEntity<JSONObject> resp = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<String>(headers), JSONObject.class);
      log.info("queryUserByToken resp:" + "    token= " + token + "  data:" + JSON.toJSONString(resp.getBody()));
      JSONObject body = resp.getBody();
      if (body != null) {
        Integer code = body.getInteger("code");
        if (code == 0) {
          String data = body.getJSONObject("data").toJSONString();
          return JSONObject.parseObject(data, AuthorUserInfoBo.class);
        }
      }
    }
    return null;
  }

  public String fetchToken(Long tenantId) {
    String tokenStr = new String();
    try {
      tokenStr = token.get("token" + "-" + tenantId);
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return tokenStr;
  }

  public String genToken(String key) {
    String token = "";
    try {
      String[] split = key.split("-");
      if(split.length == 2){
        String tenant = split[1];
        long tenantId = Long.parseLong(tenant);
        if(tenantId == 1L){
          token = "7c40c396924f56fb559eddcd390a89e9";
        }else if(tenantId == 8L){
          token = "dda917a94783ca914ada5c70edfafdbe";
        }else if(tenantId == 16L){
          token = "39a94e45e34bd8beb279b6748722294b";
        }else if(tenantId == 103L){
          token = "375c4dd0dbbf936b12c3f9d664a35240";
        }else if(tenantId == 115L){
          token = "0d7ea0349e56c11d419281f3ba4203b5";
        }
      }
    }catch (Exception e){
      log.info("getToken error: " + e.getMessage());
    }
    return token;
  }

}
