package com.mzdata.label.aspect;

import com.mzdata.label.common.AuthorizationService;
import com.mzdata.label.context.UserInfoHolder;
import com.mzdata.label.model.bo.AuthorUserInfoBo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:18
 */
@Slf4j
@Aspect
@Component
public class AuthBaseAspect {
    private static final Logger AccLog = LoggerFactory.getLogger("accessDataLog");
    private static final Logger BizLog = LoggerFactory.getLogger(AuthBaseAspect.class);

    @Resource
    private AuthorizationService authorizationService;

    @Pointcut("execution(* com.mzdata.label.controller..*.*(..))")
    private void aspect() {
    }

    @Around("aspect()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        AccLog.info("Class Method   : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes sra = (ServletRequestAttributes) requestAttributes;
            HttpServletRequest req = sra.getRequest();
            String token = req.getHeader("Authorization");
            AuthorUserInfoBo bo = authorizationService.getAuthUserInfo(token);
            //放置线程内变量
            UserInfoHolder.setInfo(bo);

            return joinPoint.proceed();
        }catch (Exception e){
            log.error("AuthBaseAspect error：" + e.getMessage());
        }finally {
            UserInfoHolder.remove();
        }

        return null;
    }

    @Before("aspect()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        String traceId = String.valueOf(UUID.randomUUID());
        MDC.put("traceId", traceId);
    }

    @After("aspect()")
    public void doAfter() throws Throwable {
        MDC.clear();
    }
}
