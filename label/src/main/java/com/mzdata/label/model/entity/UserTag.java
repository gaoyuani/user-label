package com.mzdata.label.model.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/22 17:05
 */

@Data
public class UserTag {

    private long id;

    private long tenantId;

    private long categoryId;

    private String categoryName;

    private Integer type;

    private String name;

    private String title;

    private String comment;

    private String dataType;

    private String dataDesc;

    private String tableName;

    private String columnName;

    private Integer computeMethod;

    private String sqlTemplate;

    private String sqlParameters;

    private String sqlValues;

    private String sql;

    private String sourceType;

    private long fromId;

    private String ruleContent;

    private Integer userCount;

    private BigDecimal userPercentage;

    private Integer status;

    private long createUserId;

    private String createUserName;

    private long updateUserId;

    private String updateUserName;

    private Date createdAt;

    private Date updatedAt;
}
