package com.mzdata.label.model.param;

import lombok.Data;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/20 12:20
 */

@Data
public class  TagCategoryParam {

    private Long tenantId;

    private String name;

    private long parentId;

    private Integer hasBaseTag;

    private List<Long> baseTagIds;
}
