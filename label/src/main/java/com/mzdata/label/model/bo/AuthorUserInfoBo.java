package com.mzdata.label.model.bo;

import lombok.Data;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 11:28
 */
@Data
public class AuthorUserInfoBo {
    private long id;

    private long tenantId;

    private String tenantCode;

    private String tenantName;

    private String tenantLogo;

    private String realName;

    private String avatar;

    private String phone;

    private String mail;
}
