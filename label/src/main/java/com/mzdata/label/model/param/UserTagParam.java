package com.mzdata.label.model.param;

import lombok.Data;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 16:04
 */

@Data
public class UserTagParam {

    private long id;

    private long fromId;

    private String str;
}
