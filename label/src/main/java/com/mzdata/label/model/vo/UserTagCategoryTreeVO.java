package com.mzdata.label.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/22 20:20
 */

@Data
public class UserTagCategoryTreeVO {

    private long id;

    private long tenantId;

    private long parentId;

    private Integer type;

    private Integer level;

    private String name;

    private List<UserTagCategoryTreeVO> children;
}
