package com.mzdata.label.model.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 16:25
 */
@Data
public class BasicUserTagVO {
    private long id;

    private long tenantId;

    private long categoryId;

    private String categoryName;

    private String title;

    private String sqlTemplate;

    private String sqlParameters;

    private Integer userCount;

    private BigDecimal userPercentage;
}
