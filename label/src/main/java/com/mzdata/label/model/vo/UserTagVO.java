package com.mzdata.label.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/23 12:27
 */
@Data
public class UserTagVO {

    private long value;

    private String label;

    private List<UserTagView> list;

    @Data
    public static class UserTagView {
        private long value;

        private long tenantId;

        private String label;

        private String desc;
    }
}
