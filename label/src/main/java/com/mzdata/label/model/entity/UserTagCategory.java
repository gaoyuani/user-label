package com.mzdata.label.model.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author ：Winter-Soldier
 * @description：
 * @date ：2021/7/22 14:24
 */

@Data
public class UserTagCategory {

    private long id;

    private long tenantId;

    private long parentId;

    private Integer type;

    private Integer level;

    private Integer sortValue;

    private String name;

    private Integer hasBaseTag;

    private String baseTagIds;

    private Integer userCount;

    private BigDecimal userPercentage;

    private Integer status;

    private long createUserId;

    private String createUserName;

    private long updateUserId;

    private String updateUserName;

    private Date createdAt;

    private Date updatedAt;

    private List<UserTagCategory> children;
}
